package pl.edu.mimuw.bm347174.niezmiennyzbior;

import java.util.Collection;
import java.util.Iterator;

import pl.edu.mimuw.bm347174.drzewa.*;

public class DrzewowyNiezmiennyZbior<T extends Comparable<T>> implements NiezmiennyZbior<T> {
	private Drzewo<T> d;
	private int rozmiar;
	
	public DrzewowyNiezmiennyZbior(Collection<T> kolekcja) {
		d = new Drzewo<T>();
		rozmiar = d.policzWęzły();
		
		for ( T t : kolekcja ) {
			d.dodaj(t);
		}
	}
	
	@Override
	public boolean czyZawiera(T obiekt) {
		return d.czyZawiera(obiekt);
	}

	@Override
	public int rozmiar() {
		return rozmiar;
	}

	@Override
	public boolean czyPuste() {
		return d.czyPuste();
	}

	@Override
	public Iterator<T> iterator() {
		return null;
	}

}

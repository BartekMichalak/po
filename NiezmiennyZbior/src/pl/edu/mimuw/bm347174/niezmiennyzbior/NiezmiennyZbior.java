package pl.edu.mimuw.bm347174.niezmiennyzbior;

import java.util.Iterator;

public interface NiezmiennyZbior<T> {
	
	boolean czyZawiera(T obiekt);
	int rozmiar();
	boolean czyPuste();
	Iterator<T> iterator();
}

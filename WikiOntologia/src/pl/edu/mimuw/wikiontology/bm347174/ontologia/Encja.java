package pl.edu.mimuw.wikiontology.bm347174.ontologia;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class Encja {
	protected String nazwa;
	protected HashMap<String, Encja> powiazania = new HashMap<String, Encja>();
	protected List<String> kategorie = new ArrayList<String>();
	public Encja(String nazwa) { this.nazwa = nazwa; }
	public String dajNazwe() { return nazwa; }
	public boolean czyWKategorii( String nazwaKategorii ) { return kategorie.contains(nazwaKategorii);	}
	public HashMap<String, Encja> dajPowiazania() { return (HashMap<String, Encja>) powiazania.clone(); }
	public void dodajPowiazanie(String nazwa, Encja encja) { powiazania.put(nazwa, encja); }
	public void usunPowiazanie(String nazwa) { powiazania.remove(nazwa); }
	public void dodajKategorie(String kategoria) { kategorie.add(kategoria); }
}

package pl.edu.mimuw.wikiontology.bm347174.ontologia;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class Sprawdzacz {
	protected List<String> listaWzorow = new ArrayList<String>();
	
	public boolean czySieZgadza(String tekst) {
		Pattern pattern;
		Matcher matcher;
		boolean czyZgadza = false;
		for (String s : listaWzorow) {
			pattern = Pattern.compile(s);
			matcher = pattern.matcher(tekst);
			if (matcher.find()) {
				czyZgadza = true;
				break;
			}
		}
		return czyZgadza;
	}
}

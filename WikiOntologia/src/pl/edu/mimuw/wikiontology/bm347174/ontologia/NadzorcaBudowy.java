package pl.edu.mimuw.wikiontology.bm347174.ontologia;

public class NadzorcaBudowy {
	private BudowniczyOntologii bo;
	
	public void ustawBudowniczego(BudowniczyOntologii budowniczy) {
		bo = budowniczy;
	}
	
	public WikiOntologia dajOntologie() {
		return bo.dajOntologie();
	}
	
	public void nakazBudowe() {
		bo.nowaOntologia();
		bo.zbudujGrafPowiazan();
		bo.zbudujListeKategorii();
		bo.zbudujEncje();
	}
}

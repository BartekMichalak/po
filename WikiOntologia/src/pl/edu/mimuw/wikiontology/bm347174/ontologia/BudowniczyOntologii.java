package pl.edu.mimuw.wikiontology.bm347174.ontologia;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.helpers.DefaultHandler;

public abstract class BudowniczyOntologii {
	protected SAXParserFactory fabryka = SAXParserFactory.newInstance();
	protected SAXParser saxParser;
	protected DefaultHandler obsluga;
	protected String plikZrodlowy;
	protected CzytnikDanych czytnik;
	protected WikiOntologia ontologia;
	
	public void nowaOntologia() {
		ontologia = new WikiOntologia();
	}
	public WikiOntologia dajOntologie() {
		return ontologia;
	}
	
	public abstract void zbudujGrafPowiazan();
	public abstract void zbudujEncje();
	public abstract void zbudujListeKategorii();
	public void ustawPlikZrodlowy(String s) {
		plikZrodlowy = s;
	}
}

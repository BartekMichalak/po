package pl.edu.mimuw.wikiontology.bm347174.ontologia;

import java.util.List;

public class WikiOntologia {
	protected GrafPowiazan<Encja> graf;
	protected List<String> mozliweKategorie;

	public void wypiszCoSieDa() {
		graf.wypisz();
	}
	
	public void wypiszNajkrotszaSciezke(String encja1, String encja2, String filtrKategorii) {
		if (!graf.posiadaEncje(encja1) || !graf.posiadaEncje(encja2)
			|| (!mozliweKategorie.contains(filtrKategorii) && filtrKategorii != null)) {
			System.out.println("Nie ma encji w ontologii odpowiadających zapytaniu.");
			return ;
		}
		List<Encja> wyniki = graf.dajNajkrotszaSciezke(encja1, encja2, filtrKategorii);
		if (wyniki.size() == 0) {
			System.out.println("Ścieżka między podanymi encjami nie istnieje.");
			return ;
		}
		System.out.println("***");
		System.out.println("Path length: " + String.valueOf(wyniki.size()-1));
		for (Encja e : wyniki) {
			System.out.println(e.dajNazwe());
		}
		System.out.println("***");
	}
	
	public void wypiszNajkrotszaSciezke(String encja1, String encja2) {
		wypiszNajkrotszaSciezke(encja1, encja2, null);
	}
	public void ustawGraf(GrafPowiazan<Encja> graf) {
		this.graf = graf;
	}
	public void ustawKategorie(List<String> kategorie) {
		mozliweKategorie = kategorie;
	}
	public void dodajEncje(Encja e) {
		graf.dodajEncje(e);
	}
	public int liczbaElementow() {
		return graf.liczbaElementow();
	}
}

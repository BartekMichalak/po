package pl.edu.mimuw.wikiontology.bm347174.ontologia;

import java.util.HashMap;

import org.xml.sax.helpers.DefaultHandler;

public abstract class CzytnikDanych extends DefaultHandler {
	public abstract HashMap<String, Encja> dajEncje();
}

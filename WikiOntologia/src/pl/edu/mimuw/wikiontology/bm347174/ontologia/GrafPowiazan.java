package pl.edu.mimuw.wikiontology.bm347174.ontologia;

import java.util.*;

public class GrafPowiazan<TypEncji extends Encja> {
	private HashMap<String, TypEncji> encje = new HashMap<String, TypEncji>();
	
	public boolean posiadaEncje(String nazwaEncji) { return encje.containsKey(nazwaEncji); }
	public TypEncji dajEncje(String nazwaEncji) { return encje.get(nazwaEncji); }
	public void dodajEncje(TypEncji e) { encje.put(e.dajNazwe().toLowerCase(), e); }
	
	public List<TypEncji> dajNajkrotszaSciezke(String nazwaEncjiPoczatkowej, String nazwaEncjiKoncowej, String filtrKategorii) {
		Queue<Encja> kolejka = new LinkedList<Encja>();
		List<TypEncji> wynik = new LinkedList<TypEncji>();
		Set<Encja> odwiedzone = new HashSet<Encja>();
		HashMap<Encja, Encja> poprzednik = new HashMap<Encja, Encja>();
		Encja pom = dajEncje(nazwaEncjiPoczatkowej.toLowerCase());
		kolejka.add(pom);
		odwiedzone.add(pom);
		poprzednik.put(pom, null);
		boolean znaleziono = false;
		if (filtrKategorii == null || pom.czyWKategorii(filtrKategorii)) {
			while (!kolejka.isEmpty()) {
				pom = kolejka.poll();
				if (pom.dajNazwe().toLowerCase().equals(nazwaEncjiKoncowej.toLowerCase())) {
					znaleziono = true;
					break;
				}
				for (Encja e : pom.dajPowiazania().values()) {
					if (!odwiedzone.contains(e) && (filtrKategorii == null || e.czyWKategorii(filtrKategorii))) {
						odwiedzone.add(e);
						kolejka.add(e);
						poprzednik.put(e, pom);
					}
				}
			}
		}
		if (znaleziono) {
			while (pom != null) {
				wynik.add(0, (TypEncji) pom);
				pom = poprzednik.get(pom);
			}
		}
		return wynik;
	}
	
	public void wypisz() {
		for (String s : encje.keySet()) {
			System.out.println(s);
		}
	}
	
	public int liczbaElementow() {
		return encje.size();
	}
	
}

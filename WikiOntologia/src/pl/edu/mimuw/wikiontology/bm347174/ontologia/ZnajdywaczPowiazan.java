package pl.edu.mimuw.wikiontology.bm347174.ontologia;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ZnajdywaczPowiazan {
	private static final Pattern pattern1 = Pattern.compile("\\[\\[([^\\||^\\]]*)\\|([^\\]]*)\\]\\]");
	private static final Pattern pattern2 = Pattern.compile("\\[\\[([a-zA-Z0-9]*|[^\\]|^\\|]*)\\]\\]");
	private Matcher matcher1, matcher2;
	
	public List<String> znajdzPowiazania(String tekst) {
		List<String> powiazania = new ArrayList<String>();
		String s1;
		matcher2 = pattern2.matcher(tekst);
		matcher1 = pattern1.matcher(tekst);
		while (matcher1.find()) {
			s1 = matcher1.group(1);
			powiazania.add(s1);
		}
		while(matcher2.find()) {
			s1 = matcher2.group(1);
			powiazania.add(s1);			
		}
		return powiazania;
	}
}

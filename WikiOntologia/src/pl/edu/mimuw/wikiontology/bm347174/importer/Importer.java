package pl.edu.mimuw.wikiontology.bm347174.importer;

import pl.edu.mimuw.wikiontology.bm347174.historycznepostaci.OntologiaPostaciHistorycznychBud;
import pl.edu.mimuw.wikiontology.bm347174.ontologia.BudowniczyOntologii;
import pl.edu.mimuw.wikiontology.bm347174.ontologia.NadzorcaBudowy;
import pl.edu.mimuw.wikiontology.bm347174.ontologia.WikiOntologia;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Importer {
	public static void main(String[] args) {
		try {
		NadzorcaBudowy nadzorca = new NadzorcaBudowy();
		BudowniczyOntologii bud = new OntologiaPostaciHistorycznychBud();
		bud.ustawPlikZrodlowy(args[0]);
		nadzorca.ustawBudowniczego(bud);
		nadzorca.nakazBudowe();
		WikiOntologia o = nadzorca.dajOntologie();
		Scanner s = new Scanner(System.in);
		String polecenie, odKogo, doKogo, filtrZapytania;
		StringTokenizer st;
		while (s.hasNext()) {
			polecenie = s.nextLine();
			st = new StringTokenizer(polecenie);
			odKogo = st.nextToken().toLowerCase().replaceAll("_", " ");
			doKogo = st.nextToken().toLowerCase().replaceAll("_", " ");
			filtrZapytania = st.nextToken().toLowerCase().replaceAll("_", " ");
			if (filtrZapytania.equals("all")) {
				o.wypiszNajkrotszaSciezke(odKogo, doKogo);
			} else {
				o.wypiszNajkrotszaSciezke(odKogo, doKogo, filtrZapytania);
			}
		}
		s.close();
		} catch (Exception e) {
			System.out.println("Blad. Sprawdz sciezke pliku i wydane polecenia.");
		}
	}
}

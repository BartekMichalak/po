package pl.edu.mimuw.wikiontology.bm347174.historycznepostaci;

import pl.edu.mimuw.wikiontology.bm347174.ontologia.Sprawdzacz;

public class SprawdzaczOsoby extends Sprawdzacz {
	
	public SprawdzaczOsoby() {
		listaWzorow.add("\\[\\[Category:([^\\]]*)births([^\\]]*)\\]\\]");
		listaWzorow.add("\\[\\[Category:([^\\]]*)individuals([^\\]]*)\\]\\]");
		listaWzorow.add("\\[\\[Category:([^\\]]*)philosophers([^\\]]*)\\]\\]");
		listaWzorow.add("\\[\\[Category:([^\\]]*)physicists([^\\]]*)\\]\\]");
		listaWzorow.add("\\[\\[Category:([^\\]]*)deaths([^\\]]*)\\]\\]");
		listaWzorow.add("\\[\\[Category:([^\\]]*)mathematicians([^\\]]*)\\]\\]");
		listaWzorow.add("\\{\\{Persondata([^\\}]*)");
	}

}

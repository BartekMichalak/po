package pl.edu.mimuw.wikiontology.bm347174.historycznepostaci;

import pl.edu.mimuw.wikiontology.bm347174.ontologia.Encja;

public class EncjaCzlowieka extends Encja {
	public EncjaCzlowieka(String nazwa) {
		super(nazwa);
	}
	protected String imie = nazwa.replaceAll("_", " ");
	public String dajImie() { return imie; }
	@Override
	public String dajNazwe() { return dajImie(); }
}

package pl.edu.mimuw.wikiontology.bm347174.historycznepostaci;

import java.io.IOException;
import java.util.*;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import pl.edu.mimuw.wikiontology.bm347174.ontologia.BudowniczyOntologii;
import pl.edu.mimuw.wikiontology.bm347174.ontologia.Encja;
import pl.edu.mimuw.wikiontology.bm347174.ontologia.GrafPowiazan;

public class OntologiaPostaciHistorycznychBud extends BudowniczyOntologii {
	@Override
	public void zbudujGrafPowiazan() {
		ontologia.ustawGraf(new GrafPowiazan<Encja>());

	}

	@Override
	public void zbudujEncje() {
		try {
			saxParser = fabryka.newSAXParser();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}
		czytnik = new CzytnikDanychOsob();
		try {
			saxParser.parse(plikZrodlowy, czytnik);
			HashMap<String, Encja> mapaEncji = czytnik.dajEncje();
			for (String s : mapaEncji.keySet()) {
				ontologia.dodajEncje((EncjaCzlowieka) mapaEncji.get(s));
				for (String s2 : mapaEncji.get(s).dajPowiazania().keySet()) {
					if (mapaEncji.keySet().contains(s2.toLowerCase())) {
						mapaEncji.get(s).usunPowiazanie(s2.toLowerCase());
						mapaEncji.get(s).dodajPowiazanie(s2.toLowerCase(), (EncjaCzlowieka) mapaEncji.get(s2)); 
					} else {
						mapaEncji.get(s).usunPowiazanie(s2.toLowerCase());
					}
				}
			}
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Blad wejscia. Sprawdz poprawnosc sciezki.");
		}

	}
	
	@Override
	public void zbudujListeKategorii() {
		ArrayList<String> kategorie = new ArrayList<String>();
		kategorie.add("physicist");
		ontologia.ustawKategorie(kategorie);

	}

}

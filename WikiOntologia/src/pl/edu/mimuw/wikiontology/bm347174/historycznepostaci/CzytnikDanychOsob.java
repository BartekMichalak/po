package pl.edu.mimuw.wikiontology.bm347174.historycznepostaci;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import pl.edu.mimuw.wikiontology.bm347174.ontologia.CzytnikDanych;
import pl.edu.mimuw.wikiontology.bm347174.ontologia.Encja;
import pl.edu.mimuw.wikiontology.bm347174.ontologia.Sprawdzacz;
import pl.edu.mimuw.wikiontology.bm347174.ontologia.ZnajdywaczPowiazan;

public class CzytnikDanychOsob extends CzytnikDanych {
	private List<String> powiazania = new ArrayList<String>();
	private String dana, nazwaArtykulu;
	private String tekstArtykulu = "";
	private Encja en;
	private HashMap<String, Encja> mapaEncji = new HashMap<String, Encja>();
	private Sprawdzacz sprawdzaczOsob = new SprawdzaczOsoby();
	private Sprawdzacz sprawdzaczFizykow = new SprawdzaczFizyka();
	private boolean czyToOsoba = false;
	private boolean czyToFizyk = false;
	private ZnajdywaczPowiazan zp = new ZnajdywaczPowiazan();
	
	public void startElement(String uri, String localName,String qName, 
                Attributes attributes) throws SAXException {
 
		dana = qName;
		if (dana.equals("text")) {
			tekstArtykulu = "";
		}
 
	}
 
	public void endElement(String uri, String localName,
		String qName) throws SAXException {
		if (dana.equals("text")) {
			if (czyToOsoba) {
				en = new EncjaCzlowieka(nazwaArtykulu);
				if (czyToFizyk) {
					en.dodajKategorie("physicist");
				}
				for (String s : powiazania) {
					en.dodajPowiazanie(s.toLowerCase(), null);
				}
				mapaEncji.put(nazwaArtykulu.toLowerCase(), en);
			}
			
			czyToFizyk = false;
			czyToOsoba = false;
			powiazania.removeAll(powiazania);			
		}
		dana = "";
 
	}
 
	public void characters(char ch[], int start, int length) throws SAXException {

		
		if (dana.equals("text")) {
			tekstArtykulu = new String(ch, start, length);
			czyToOsoba = czyToOsoba || sprawdzaczOsob.czySieZgadza(tekstArtykulu);
			czyToFizyk = czyToFizyk || sprawdzaczFizykow.czySieZgadza(tekstArtykulu);
			powiazania.addAll(zp.znajdzPowiazania(tekstArtykulu));
		} else if (dana.equals("title")) {
			nazwaArtykulu = new String(ch, start, length);
		}
 
	}
	@Override
	public HashMap<String, Encja> dajEncje() { return mapaEncji; }
}

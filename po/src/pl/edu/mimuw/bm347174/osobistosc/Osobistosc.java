package pl.edu.mimuw.bm347174.osobistosc;

public class Osobistosc {

	public static void main(String[] args) {
	int tab[][] = new int[10][10];
	// tu dodajemy cos na inicjacje wartosci w tablicy
	if (osobistosc(tab))
		System.out.println("Jest osobistosc.");
	else
		System.out.println("Nie ma osobistosci.");
	}
	
	public static boolean osobistosc(int[][] zna) {
		if (zna.length < 1) {
			return false;
		}
		int k, l;
		for (k = 0, l = 0; (k < zna[0].length) && (l < zna.length);) {
			if (zna[l][k] == 0) {
				k++;
			} else {
				l++;
			}
		}
		int i = 0;
		int j = 0;
		while (i < zna.length && zna[i][k] == 1) {
			i++;
		}
		for (j = 0; j < zna[0].length;) {
			if (j==k || zna[k][j] == 0) j++; else break;
		}
		return (i == zna.length && j == zna[0].length);
	}

}

package pl.edu.mimuw.bm347174.grafy_kontenery;

import java.lang.reflect.Array;
import java.util.Arrays;


public class Stos<T> implements Kontener<T> {
	private int maxRozmiar = 100;
	private T[] sterta;
	private int obecnyIndeks = 0;
	
	public Stos(T[] tab) {
		sterta = Arrays.copyOf(tab, maxRozmiar);
	}
	
	public Stos(Class<T> klasa) {
		sterta = (T[]) Array.newInstance(klasa, maxRozmiar);
	}
	
	public boolean czyPusty() {
		return ((sterta.length == 0) || (obecnyIndeks == 0));
	}
	
	public T pobierz() {
		if (!czyPusty()) {
		return sterta[--obecnyIndeks];
		} else {
			System.out.println("Stos jest pusty.");			
			return null;
		}
	}
	
	public void wstaw(T t) {
		if (obecnyIndeks >= maxRozmiar-1) {
			System.out.println("Przekroczono zakres.");
		} else {
			sterta[obecnyIndeks++] = t;
		}
	}
	
	
}

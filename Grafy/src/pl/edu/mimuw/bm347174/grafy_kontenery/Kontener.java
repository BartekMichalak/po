package pl.edu.mimuw.bm347174.grafy_kontenery;


public interface Kontener <T> {
	public void wstaw(T t);
	public T pobierz();
	public boolean czyPusty();
}

package pl.edu.mimuw.bm347174.grafy_kontenery;


public class Wierzchołek {
	private Wierzchołek[] sąsiedzi = new Wierzchołek[9];
	private boolean odwiedzony = false;
	private int wartość;
	private int indeksSąsiada = 0;
	
	public void zaznaczOdwiedzony() {
		odwiedzony = true;
	}
	
	public void zaznaczNieodwiedzony() {
		odwiedzony = false;
	}
	
	public Wierzchołek[] pobierzSąsiadów()
	{
		return sąsiedzi;
	}
	
	public int liczbaSąsiadów() {
		return indeksSąsiada;
	}
	
	public boolean czyOdwiedzony() {
		return odwiedzony;
	}
	
	public void wstawWartość(int v) {
		wartość = v;
	}
	
	public int pobierzWartość() {
		return wartość;
	}
	
	public void dodajSąsiada(Wierzchołek w) {
		boolean czyJuzJest = false;
		for (Wierzchołek s:sąsiedzi) {
			if (s==w) {
				czyJuzJest = true;
				break;
			}
		}
		if (!czyJuzJest) {
		sąsiedzi[indeksSąsiada++] = w;
		w.dodajSąsiada(this);
		}
	}
	
}

package pl.edu.mimuw.bm347174.grafy_kontenery;

import java.lang.reflect.Array;
import java.util.Arrays;


public class Kolejka<T> implements Kontener<T> {
	private int maxRozmiar = 100;
	private T[] sterta;
	private int indeksPoczatkowy = 0;
	private int iloscElementow = 0;
	
	public Kolejka(T[] tab) {
		sterta = Arrays.copyOf(tab, maxRozmiar);
	}
	
	public Kolejka(Class<T> klasa) {
		sterta = (T[]) Array.newInstance(klasa, maxRozmiar);
	}
	
	public void wstaw(T t) {
		if (!czyPelny()) {
			sterta[(++iloscElementow+indeksPoczatkowy-1)%maxRozmiar] = t;
		}

	}

	public T pobierz() {
		int i = indeksPoczatkowy;
		indeksPoczatkowy = (++indeksPoczatkowy)%maxRozmiar;
		iloscElementow--;
		return sterta[i];
	}

	public boolean czyPusty() {
		
		return (iloscElementow == 0);
	}
	
	public boolean czyPelny() {
		return (iloscElementow == maxRozmiar);
	}

}

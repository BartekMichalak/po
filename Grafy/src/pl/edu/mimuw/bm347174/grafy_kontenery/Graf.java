package pl.edu.mimuw.bm347174.grafy_kontenery;


public class Graf {
	private int liczbaWierzchołków = 10;
	private Wierzchołek[] wierzchołki = new Wierzchołek[liczbaWierzchołków];
	
	public Wierzchołek pobierzPierwszy() {
		return wierzchołki[0];
	}
	
	public void zerujOdwiedzone() {
		for (int i=0; i < liczbaWierzchołków; i++) {
			wierzchołki[i].zaznaczNieodwiedzony();
		}
	}
	
	public int rozmiarGrafu() {
		return liczbaWierzchołków;
	}

	public Graf() {
		for (int i=0; i < liczbaWierzchołków; i++) {
			wierzchołki[i] = new Wierzchołek();
			wierzchołki[i].zaznaczNieodwiedzony();
			wierzchołki[i].wstawWartość(i);
			for (int j=0; j < i; j++) {
				wierzchołki[i].dodajSąsiada(wierzchołki[j]);
			}
		}
	}
}

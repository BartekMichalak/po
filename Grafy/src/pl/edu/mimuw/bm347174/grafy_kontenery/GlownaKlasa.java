package pl.edu.mimuw.bm347174.grafy_kontenery;


public class GlownaKlasa {

	public static void main(String[] args) {
		Graf graf = new Graf();
		int[] tab = new int[graf.rozmiarGrafu()];
		tab = przejścieGrafu(graf, new Kolejka<Wierzchołek>(new Wierzchołek[0]));
		for (int i=0; i<graf.rozmiarGrafu(); i++) {
			System.out.print(tab[i]);
		}
		System.out.println();
		tab = przejścieGrafu(graf, new Stos<Wierzchołek>(new Wierzchołek[0]));
		for (int i=0; i<graf.rozmiarGrafu(); i++) {
			System.out.print(tab[i]);
		}
	}

	public static int[] przejścieGrafu(Graf g, Kontener<Wierzchołek> kontener) {

		Wierzchołek w;
		int[] wyniki = new int[g.rozmiarGrafu()];
		int i = 0;
		g.zerujOdwiedzone();
		w = g.pobierzPierwszy();
		kontener.wstaw(w);
		w.zaznaczOdwiedzony();
		while (!kontener.czyPusty()) {
			w = kontener.pobierz();
			Wierzchołek[] sąsiedzi = w.pobierzSąsiadów();
			wyniki[i++] = w.pobierzWartość();
			for (int j=0; j < w.liczbaSąsiadów(); j++) {
				Wierzchołek s = sąsiedzi[j];
				if (!s.czyOdwiedzony()) {
					kontener.wstaw(s);
					s.zaznaczOdwiedzony();
				}
			}
		}
		return wyniki;
	}
}

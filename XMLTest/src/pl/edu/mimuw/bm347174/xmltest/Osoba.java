package pl.edu.mimuw.bm347174.xmltest;

import java.util.HashMap;

public class Osoba {
	private HashMap<String, String> dane = new HashMap<String, String>();
	public HashMap<String, String> dajDane() {
		return dane;
	}
	
	public void wypisz() {
		for (String s : dane.keySet()) {
			System.out.println(s + ": " + dane.get(s));
		}
		System.out.println(dane.get("imie"));
	}
}

package pl.edu.mimuw.bm347174.xmltest;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class CzytnikXMLa {
	private SAXParserFactory factory = SAXParserFactory.newInstance();
	private SAXParser saxParser;
	private Osoba osoba;
	private DefaultHandler handler = new DefaultHandler() {
		
		private String dana;
		
		
		public void startElement(String uri, String localName,String qName, 
	                Attributes attributes) throws SAXException {
	 
			//System.out.println("<" + qName + ">");
	 
			dana = qName;
	 
		}
	 
		public void endElement(String uri, String localName,
			String qName) throws SAXException {
			dana = "";
			//System.out.println("</" + qName + ">");
	 
		}
	 
		public void characters(char ch[], int start, int length) throws SAXException {

			/*if (!dana.isEmpty()) {
				String str = new String(ch, start, length);
				if (!dana.equals("osoba") && !dana.equals("dane"))
					osoba.dajDane().put(dana, str);
				System.out.println(str);
			}*/
			if (dana.equals("text")) {
				String text = new String(ch, start, length);
				Pattern pattern = Pattern.compile("\\[\\[Category:([^\\]]*)\\]\\]");
				Matcher matcher = pattern.matcher(text);
				while (matcher.find()) {
					System.out.println(matcher.group(1));
				}
			}
	 
		}
 
    };
    
    public CzytnikXMLa(Osoba o) {
    	osoba = o;
    	try {
			saxParser = factory.newSAXParser();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}
    }
    
    public DefaultHandler dajHandlera() {
    	return handler;
    }
    
    public SAXParser dajParser() {
    	return saxParser;
    }
}

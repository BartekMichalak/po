package pl.edu.mimuw.bm347174.wyrarytm;

import java.util.Random;

public abstract class Wyrazenie {
	abstract Wyrazenie uprosc();
	public abstract double oblicz(double i);
	public abstract void wypisz();
	protected boolean czyStala() {return false;}
	abstract Wyrazenie pochodna();
	public double calka(double poczatek, double koniec, int dokladnosc) {
		double wynik = 0;
		Random rd = new Random();
		for (int i=1; i < dokladnosc; i++) {
			wynik = (Double.valueOf(i)/Double.valueOf(i+1)) * wynik + oblicz((poczatek) + (koniec-poczatek) * rd.nextDouble())/Double.valueOf(i+1);
		}
		return wynik * (koniec - poczatek);
	}
}

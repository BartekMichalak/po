package pl.edu.mimuw.bm347174.wyrarytm;

public class Cos extends WyrazenieJednoargumentowe {

	public Cos(Wyrazenie w) {
		super(w);
	}
	@Override
	protected Wyrazenie pochodnaWlasna() {
		return new Razy(new Stala(-1.0), new Sin(srodek));
	}

	@Override
	Wyrazenie uprosc() {
		return this;
	}

	@Override
	public double oblicz(double i) {
		return Math.cos(srodek.oblicz(i));
	}

	@Override
	public void wypisz() {
		System.out.print("cos(");
		srodek.wypisz();
		System.out.print(")");
	}

}

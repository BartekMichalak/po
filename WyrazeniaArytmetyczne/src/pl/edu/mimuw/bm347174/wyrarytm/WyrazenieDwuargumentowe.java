package pl.edu.mimuw.bm347174.wyrarytm;

public abstract class WyrazenieDwuargumentowe extends Wyrazenie {
	protected Wyrazenie lewy, prawy;
	
	protected WyrazenieDwuargumentowe(Wyrazenie a, Wyrazenie b) {
		lewy = a;
		prawy = b;
	}
	protected abstract String symbol();
	public void wypisz() {
		lewy.wypisz();
		System.out.print(" " + symbol() + " ");
		prawy.wypisz();
	}
}

package pl.edu.mimuw.bm347174.wyrarytm;

public class Stala extends Wyrazenie {
	private double wartosc;
	
	public Stala(double i) {
		wartosc = i;
	}
	@Override
	Wyrazenie uprosc() {
		return this;
	}

	@Override
	public double oblicz(double i) {
		return wartosc;
	}

	@Override
	public void wypisz() {
		System.out.print(wartosc);
	}

	@Override
	Wyrazenie pochodna() {
		return new Stala(0.0);
	}
	
	public boolean czyStala() { return true; }
	
}

package pl.edu.mimuw.bm347174.wyrarytm;

public class Razy extends WyrazenieDwuargumentowe {
	private static final String RAZY = "*";
	public Razy(Wyrazenie lewe, Wyrazenie prawe) {
		super(lewe, prawe);
	}

	@Override
	protected String symbol() {
		return RAZY;
	}

	@Override
	Wyrazenie uprosc() {
		for (Wyrazenie w : (new Wyrazenie[] {lewy, prawy})) {
			if (w.czyStala() && w.oblicz(1.0) == 1.0) {
				return w;
			}
		}
		return this;
	}

	@Override
	public double oblicz(double i) {
		return lewy.oblicz(i) * prawy.oblicz(i);
	}

	@Override
	Wyrazenie pochodna() {
		
		return new Plus(new Razy(lewy.pochodna(), prawy), new Razy(lewy, prawy.pochodna()));
	}
}

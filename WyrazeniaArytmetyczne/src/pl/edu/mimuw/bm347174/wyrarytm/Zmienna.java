package pl.edu.mimuw.bm347174.wyrarytm;

public class Zmienna extends Wyrazenie {

	@Override
	Wyrazenie uprosc() {
		return this;
	}

	@Override
	public double oblicz(double i) {
		return i;
	}

	@Override
	public void wypisz() {
		System.out.print('x');

	}

	@Override
	Wyrazenie pochodna() {
		return new Stala(1.0);
	}

}

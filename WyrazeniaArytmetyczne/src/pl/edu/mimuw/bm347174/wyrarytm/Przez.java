package pl.edu.mimuw.bm347174.wyrarytm;

public class Przez extends WyrazenieDwuargumentowe {
	private static final String PRZEZ = "/";
	public Przez(Wyrazenie lewe, Wyrazenie prawe) {
		super(lewe, prawe);
	}

	@Override
	protected String symbol() {
		return PRZEZ;
	}

	@Override
	Wyrazenie uprosc() {
		if ((lewy.czyStala() && lewy.oblicz(0.0) == 0.0) || (prawy.czyStala() && prawy.oblicz(1.0) == 1.0)) {
			return lewy;
		}
		return this;
	}

	@Override
	public double oblicz(double i) {
		return lewy.oblicz(i) / prawy.oblicz(i);
	}

	@Override
	Wyrazenie pochodna() {
		
		return new Przez(new Minus(new Razy(lewy.pochodna(), prawy), new Razy(lewy, prawy.pochodna())), new Razy(prawy, prawy));
	}
}

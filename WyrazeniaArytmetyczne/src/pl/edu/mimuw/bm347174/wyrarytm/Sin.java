package pl.edu.mimuw.bm347174.wyrarytm;

public class Sin extends WyrazenieJednoargumentowe {

	public Sin(Wyrazenie w) {
		super(w);
	}
	
	@Override
	protected Wyrazenie pochodnaWlasna() {
		return new Cos(srodek);
	}

	@Override
	Wyrazenie uprosc() {
		return this;
	}

	@Override
	public double oblicz(double i) {
		return Math.sin(srodek.oblicz(i));
	}

	@Override
	public void wypisz() {
		System.out.print("sin(");
		srodek.wypisz();
		System.out.print(")");
	}
}

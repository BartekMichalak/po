package pl.edu.mimuw.bm347174.wyrarytm;

public class Plus extends WyrazenieDwuargumentowe {
	private static final String PLUS = "+";
	public Plus(Wyrazenie lewe, Wyrazenie prawe) {
		super(lewe, prawe);
	}

	@Override
	protected String symbol() {
		return PLUS;
	}

	@Override
	Wyrazenie uprosc() {
		for (Wyrazenie w : (new Wyrazenie[] {lewy, prawy})) {
			if (w.czyStala() && w.oblicz(1.0) == 0.0) {
				return w;
			}
		}
		return this;
	}

	@Override
	public double oblicz(double i) {
		return lewy.oblicz(i) + prawy.oblicz(i);
	}

	@Override
	Wyrazenie pochodna() {
		
		return new Plus(lewy.pochodna(), prawy.pochodna());
	}
}

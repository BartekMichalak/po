package pl.edu.mimuw.bm347174.wyrarytm;

public class Log extends WyrazenieJednoargumentowe {
	
	public Log(Wyrazenie w) {
		super(w);
	}
	
	@Override
	protected Wyrazenie pochodnaWlasna() {
		return new Przez(new Stala(1.0), srodek);
	}

	@Override
	Wyrazenie uprosc() {
		return this;
	}

	@Override
	public double oblicz(double i) {
		return Math.log(srodek.oblicz(i));
	}

	@Override
	public void wypisz() {
		System.out.print("ln(");
		srodek.wypisz();
		System.out.print(")");

	}

}

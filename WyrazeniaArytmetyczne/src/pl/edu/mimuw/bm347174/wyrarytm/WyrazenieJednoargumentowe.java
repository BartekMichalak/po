package pl.edu.mimuw.bm347174.wyrarytm;

public abstract class WyrazenieJednoargumentowe extends Wyrazenie {
	protected Wyrazenie srodek;
	public WyrazenieJednoargumentowe(Wyrazenie s) {
		srodek = s;
	}
	
	protected abstract Wyrazenie pochodnaWlasna();
	Wyrazenie pochodna() {
		return new Razy(this.pochodnaWlasna(), this.srodek.pochodna());
	}
}

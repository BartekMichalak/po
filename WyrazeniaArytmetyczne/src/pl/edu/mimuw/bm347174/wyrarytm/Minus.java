package pl.edu.mimuw.bm347174.wyrarytm;

public class Minus extends WyrazenieDwuargumentowe {
	private static final String MINUS = "-";
	public Minus(Wyrazenie lewe, Wyrazenie prawe) {
		super(lewe, prawe);
	}

	@Override
	protected String symbol() {
		return MINUS;
	}

	@Override
	Wyrazenie uprosc() {
		if (prawy.czyStala() && prawy.oblicz(1.0) == 0.0) {
			return lewy;
		}
		return null;
	}

	@Override
	public double oblicz(double i) {
		return lewy.oblicz(i) - prawy.oblicz(i);
	}

	@Override
	Wyrazenie pochodna() {
		
		return new Minus(lewy.pochodna(), prawy.pochodna());
	}
}

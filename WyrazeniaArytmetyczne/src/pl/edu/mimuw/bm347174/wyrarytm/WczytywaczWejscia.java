package pl.edu.mimuw.bm347174.wyrarytm;

public class WczytywaczWejscia {

	public static void main(String[] args) {
		WyrazenieJednoargumentowe w = new Cos ( new Minus( new Przez ( new Zmienna(), new Stala(2.0) ), new Stala(Math.PI) ) );
		WyrazenieJednoargumentowe w1 = new Cos ( new Stala(-Math.PI/2));
		w.wypisz();
		System.out.println();
		double d = w.oblicz(2.0);
		System.out.println(d);
		d = w1.oblicz(2.0);
		System.out.println(d);
		Zmienna x = new Zmienna();
		WyrazenieJednoargumentowe w2 = new Sin(x);
		WyrazenieJednoargumentowe w3 = new Cos(x);
		WyrazenieJednoargumentowe sin2x = new Sin( new Razy ( new Stala(2.0), x));
		WyrazenieDwuargumentowe sin2xwzor = new Razy( new Razy( new Stala(2.0), w2), w3);
		sin2x.wypisz();
		System.out.println();
		sin2xwzor.wypisz();
		System.out.println();
		System.out.println(sin2x.oblicz(7.0));
		System.out.println(sin2xwzor.oblicz(7.0));
			System.out.println("OK!");
		System.out.println(sin2x.calka(-1.0, 1.0, 100000));
	}

}

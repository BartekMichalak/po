package pl.edu.mimuw.bm347174;

public class Dzielnik extends Filtr {
	private int mojaLiczba;
	public Dzielnik(int i) {
		mojaLiczba = i;
		nastepnyFiltr = new ZaślepkaD();
	}
	
	@Override
	public void wypisz() {
		System.out.print(' ' + String.valueOf(mojaLiczba) + ' ');
		nastepnyFiltr.wypisz();

	}

	@Override
	public void weź(int i) {
		if (((mojaLiczba == 0 || i == 0) && i != mojaLiczba) || (i % mojaLiczba != 0)) {
			nastepnyFiltr.weź(i);
		}
	}

}

package pl.edu.mimuw.bm347174;

public class ZaślepkaD extends Filtr {
	@Override
	public void weź(int i) {
		if (nastepnyFiltr == null) {
			nastepnyFiltr = new Dzielnik(i);
		} else {
			nastepnyFiltr.weź(i);
		}
	}
	
	public void wypisz() {
		if (nastepnyFiltr != null)
			nastepnyFiltr.wypisz();
	}
}

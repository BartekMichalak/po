package pl.edu.mimuw.bm347174;

public class Porównywacz extends Filtr {
	private int mojaLiczba;
	
	public Porównywacz(int i) {
		mojaLiczba = i;
		nastepnyFiltr = new ZaślepkaP();
	}
	
	@Override
	public void wypisz() {
		System.out.print(' ' + String.valueOf(mojaLiczba) + ' ');
		nastepnyFiltr.wypisz();

	}

	@Override
	public void weź(int i) {
		int pom = i;
		if (i < mojaLiczba) {
			pom = mojaLiczba;
			mojaLiczba = i;
		}
		nastepnyFiltr.weź(pom);
	}

}

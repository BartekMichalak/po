package pl.edu.mimuw.bm347174;

public class ZaślepkaP extends Filtr {
	@Override
	public void weź(int i) {
		if (nastepnyFiltr == null) {
			nastepnyFiltr = new Porównywacz(i);
		} else {
			nastepnyFiltr.weź(i);
		}
	}
	
	public void wypisz() {
		if (nastepnyFiltr != null)
			nastepnyFiltr.wypisz();
	}
	
}

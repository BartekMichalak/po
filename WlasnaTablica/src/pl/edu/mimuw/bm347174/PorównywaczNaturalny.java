package pl.edu.mimuw.bm347174;

public class PorównywaczNaturalny implements Porównywacz<Liczba> {
	@Override
	public int porównaj(Liczba t1, Liczba t2) {
		if (t1 == null && t2 == null) {
			return 0;
		} else if (t1 != null && t2 == null) {
			return -1;
		} else if (t1 == null && t2 != null) {
			return 1;
		} else if (t1.dajWartość() > t2.dajWartość()) {
			return 1;
		} else if (t1.dajWartość() < t2.dajWartość()) {
			return -1;
		}
		return 0;
	};
}

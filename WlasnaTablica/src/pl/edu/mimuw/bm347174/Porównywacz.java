package pl.edu.mimuw.bm347174;

public interface Porównywacz <T> {
	public int porównaj(T t1, T t2);
}

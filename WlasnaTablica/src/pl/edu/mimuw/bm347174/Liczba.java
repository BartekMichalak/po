package pl.edu.mimuw.bm347174;

public class Liczba {
	private int wartość;
	
	public Liczba(int wart) {
		ustawWartość(wart);
	}
	
	/*@Override
	public int porównaj(Liczba t1, Liczba t2) {
		return super.porównaj(this, t2);
	}*/

	public int dajWartość() {
		return wartość;
	}

	public void ustawWartość(int wartość) {
		this.wartość = wartość;
	}
	
	@Override
	public String toString() {
		return String.valueOf(wartość);
	}

}

package pl.edu.mimuw.bm347174;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Tablica <T> {
	private T[] tab;
	
	public Tablica(T[] tab) {
		this.tab = Arrays.copyOf(tab, tab.length);
	}
	
	public Tablica(Class<T> klasa, int rozmiar) {
		tab = (T[]) Array.newInstance(klasa, rozmiar);
	}
	
	public void wstawPodIndeks(T co, int indeks) {
		tab[indeks] = co;
	}
	
	public T dajSpodIndeksu(int indeks) {
		return tab[indeks];
	}
	
	public void sortuj(Porównywacz<T> p) {
		int i,j;
		T pom;
		for	(i=0; i<tab.length; i++)
			for (j=i+1; j<tab.length; j++) {
				if (p.porównaj(tab[i], tab[j]) > 0) {
					pom = tab[i];
					tab[i] = tab[j];
					tab[j] = pom;
				}
			}
	}
	
	public int dajRozmiar() {
		return tab.length;
	}
	
	@Override
	public String toString() {
		String wynik = "Tablica:";
		for (int i=0; i<dajRozmiar(); i++) {
			if (tab[i] == null) {
				wynik = wynik + " null ";
			} else {
				wynik = wynik + " " + tab[i].toString() + " ";
			}
		}
		return wynik;
	}
}

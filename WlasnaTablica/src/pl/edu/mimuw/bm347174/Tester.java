package pl.edu.mimuw.bm347174;

import java.util.Random;

public class Tester {

	public static void main(String[] args) {
		Tablica<Liczba> tab = new Tablica<Liczba>(new Liczba[10]);
		Random r = new Random();
		for (int i=0; i<tab.dajRozmiar(); i++) {
			tab.wstawPodIndeks(new Liczba(r.nextInt(20)), i);
		}
		System.out.println(tab.toString());
		tab.sortuj(new PorównywaczNaturalny());
		System.out.println(tab.toString());
		tab.sortuj(new PorównywaczAntynaturalny());
		System.out.println(tab.toString());		
	}

}

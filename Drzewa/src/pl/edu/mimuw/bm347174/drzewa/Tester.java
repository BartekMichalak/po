package pl.edu.mimuw.bm347174.drzewa;

import java.util.Iterator;
import java.util.Scanner;

public class Tester {

	public static void main(String[] args) {
		int i;
		Scanner s = new Scanner(System.in);
		Drzewo<Integer> d = new Drzewo<Integer>();
		Suma sumuj = new Suma();
		Wypisywanie<Integer> w = new Wypisywanie<Integer>();
		i = s.nextInt();
		while (i > 0) {
			d.dodaj(i);
			i = s.nextInt();
		}
		/*
		System.out.println(d.policzWęzły());
		d.wykonajOperację(sumuj);
		System.out.println(sumuj.dajSumę());
		d.wykonajOperację(w);*/
		s.close();
		Iterator<Węzeł<Integer>> it = d.iterator();
		while (it.hasNext()) {
			System.out.print(it.next().toString() + " ");
		}
	}

}

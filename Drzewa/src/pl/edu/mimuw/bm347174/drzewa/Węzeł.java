package pl.edu.mimuw.bm347174.drzewa;

import java.util.Iterator;

public interface Węzeł<T extends Comparable<T>> {
	Para<Węzeł<T>, Integer> dodaj(T wartość);
	int policzWęzły();
	boolean czyPuste();
	void wykonajOperację(Operacja<T> o);
	boolean czyZawiera(T wartość);
	Iterator<Węzeł<T>> iterator();
}

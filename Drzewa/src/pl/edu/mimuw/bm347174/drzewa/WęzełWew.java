package pl.edu.mimuw.bm347174.drzewa;

import java.util.Iterator;
import java.util.Stack;

public class WęzełWew<T extends Comparable<T>> implements Węzeł<T> {
	private T wartość;
	private Węzeł<T> lewy, prawy;
	
	public WęzełWew(T wartość) {
		this.wartość = wartość;
		lewy = WęzełNIL.getInstance();
		prawy = WęzełNIL.getInstance();
	}
	
	@Override
	public Para<Węzeł<T>, Integer> dodaj(T wartość) {
		Para<Węzeł<T>, Integer> wynik = null;
		if (this.wartość.compareTo(wartość) > 0) {
			
			wynik = lewy.dodaj(wartość);
			lewy = wynik.dajPierwszy();
			wynik.ustawPierwszy(this);
			
		} else if (this.wartość.compareTo(wartość) < 0) {
			
			wynik = prawy.dodaj(wartość);
			prawy = wynik.dajPierwszy();
			wynik.ustawPierwszy(this);
			
		} else { 
			wynik = new Para<Węzeł<T>, Integer>(this, 0);
		}
		return wynik;
	}

	@Override
	public int policzWęzły() {
		return 1 + lewy.policzWęzły() + prawy.policzWęzły();
	}

	@Override
	public boolean czyPuste() {
		return false;
	}

	@Override
	public void wykonajOperację(Operacja<T> o) {
		o.wykonajDlaWew(this);
		lewy.wykonajOperację(o);
		prawy.wykonajOperację(o);
		
	}
	
	public T dajWartość() {
		return wartość;
	}

	@Override
	public String toString() {
		return wartość.toString();
	}

	@Override
	public boolean czyZawiera(T wartość) {
		if (this.wartość.compareTo(wartość) < 0) {
			return lewy.czyZawiera(wartość);
		} else if (this.wartość.compareTo(wartość) > 0) {
			return prawy.czyZawiera(wartość);
		}
		return true;
	}

	@Override
	public Iterator<Węzeł<T>> iterator() {
		return new DrzewoIterator(this);
	}
	
	
	
	public class DrzewoIterator implements Iterator<Węzeł<T>> {
		private Stack<WęzełWew<T>> sterta = new Stack<WęzełWew<T>>();
		
		private void maxWLewo(Węzeł<T> w) {
			WęzełWew<T> pom;
			if (!w.czyPuste()){
			pom = (WęzełWew<T>) w;
			sterta.push(pom);
			while (!pom.lewy.czyPuste()) {
				pom = (WęzełWew<T>) pom.lewy;
				sterta.push(pom);
			}
			}
		}
		public DrzewoIterator(Węzeł<T> w) {
			maxWLewo(w);
		}
		
		@Override
		public boolean hasNext() {
			return (!sterta.empty());
		}
		
		

		@Override
		public Węzeł<T> next() {
			WęzełWew<T> nastepny = sterta.pop();
			if (!nastepny.prawy.czyPuste())
				maxWLewo((WęzełWew<T>)nastepny.prawy);
			return nastepny;
		}

		@Override
		public void remove() {
			// TODO Auto-generated method stub
			
		}
		
	}
	

}

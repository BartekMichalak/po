package pl.edu.mimuw.bm347174.drzewa;

public class Wypisywanie<T extends Comparable<T>> implements Operacja<T> {

	@Override
	public void wykonajDlaWew(WęzełWew<T> w) {
		System.out.print(w.toString() + " ");
		
	}

	@Override
	public void wykonajDlaNil(WęzełNIL<T> w) {
		
		
	}

}

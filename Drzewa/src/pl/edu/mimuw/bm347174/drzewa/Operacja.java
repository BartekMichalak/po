package pl.edu.mimuw.bm347174.drzewa;

public interface Operacja<T extends Comparable<T>> {
	void wykonajDlaWew(WęzełWew<T> w);
	void wykonajDlaNil(WęzełNIL<T> w);
}

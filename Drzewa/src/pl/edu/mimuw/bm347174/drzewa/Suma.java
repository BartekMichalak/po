package pl.edu.mimuw.bm347174.drzewa;

public class Suma implements Operacja<Integer> {
	private int suma = 0;
	@Override
	public void wykonajDlaWew(WęzełWew<Integer> w) {
		suma += w.dajWartość();
	}

	@Override
	public void wykonajDlaNil(WęzełNIL<Integer> w) {}
	
	public int dajSumę() {
		return suma;
	}
}

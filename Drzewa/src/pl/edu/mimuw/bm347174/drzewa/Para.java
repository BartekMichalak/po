package pl.edu.mimuw.bm347174.drzewa;

public class Para<T1, T2>{
	private T1 t1;
	private T2 t2;
	public Para(T1 pierwszy, T2 drugi) {
		t1 = pierwszy;
		t2 = drugi;
	}
	
	public T1 dajPierwszy() {
		return t1;
	}
	
	public T2 dajDrugi() {
		return t2;
	}
	
	public void ustawPierwszy(T1 pierwszy) {
		t1 = pierwszy;
	}
	
	public void ustawDrugi(T2 drugi) {
		t2 = drugi;
	}	
}

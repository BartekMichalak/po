package pl.edu.mimuw.bm347174.drzewa;

import java.util.Iterator;

public class Drzewo<T extends Comparable<T>> implements Węzeł<T> {
	private Węzeł<T> korzeń;
	private int rozmiar;
	public Drzewo() {
		rozmiar = 0;
		korzeń = WęzełNIL.getInstance();
	}
	@Override
	public Para<Węzeł<T>, Integer> dodaj(T wartość) {
		Para<Węzeł<T>, Integer> para = korzeń.dodaj(wartość);
		korzeń = para.dajPierwszy();
		rozmiar += para.dajDrugi();
		return null;
	}
	@Override
	public int policzWęzły() {
		return rozmiar;
	}
	@Override
	public boolean czyPuste() {
		return korzeń.czyPuste();
	}
	@Override
	public void wykonajOperację(Operacja<T> o) {
		korzeń.wykonajOperację(o);
		
	}
	@Override
	public boolean czyZawiera(T wartość) {
		return korzeń.czyZawiera(wartość);
	}
	@Override
	public Iterator<Węzeł<T>> iterator() {
		return korzeń.iterator();
	}
}

package pl.edu.mimuw.bm347174.drzewa;

import java.util.Iterator;

public class WęzełNIL<T extends Comparable<T>> implements Węzeł<T> {
	private static WęzełNIL instance = new WęzełNIL();
	
	private WęzełNIL() {}
	public static WęzełNIL getInstance() {
		return instance;
	}
	
	@Override
	public Para<Węzeł<T>, Integer> dodaj(T wartość) {
		return new Para(new WęzełWew<T>(wartość), 1);
	}

	@Override
	public int policzWęzły() {
		return 0;
	}

	@Override
	public boolean czyPuste() {
		return true;
	}

	@Override
	public void wykonajOperację(Operacja<T> o) {
		o.wykonajDlaNil(this);
		
	}
	
	@Override
	public String toString() {
		return "";
	}
	@Override
	public boolean czyZawiera(T wartość) {
		return false;
	}
	@Override
	public Iterator<Węzeł<T>> iterator() {
		return null;
	}

}
